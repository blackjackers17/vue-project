# Proyecto Vue.js

Este es un proyecto desarrollado con Vue.js. A continuación se detallan los pasos necesarios para configurar y ejecutar el proyecto en un entorno de desarrollo.

## Requisitos

Asegúrate de tener instaladas las siguientes herramientas:

- Node.js versión 12.14.1 o superior.
- npm (incluido con Node.js)

## Instalación

Sigue los siguientes pasos para instalar y configurar el proyecto:

1. **Clona el repositorio:**

   ```bash
   git clone https://github.com/tu-usuario/tu-proyecto-vue.git
   cd tu-proyecto-vue

2. **Instala las dependencias:**

   ```bash
   npm install

3. **Para ejecutar el servidor de desarrollo, usa el siguiente comando:**

   ```bash
   npm run dev


### Descripción:

1. **Requisitos**:
   - Se especifica que la versión de Node.js utilizada es la 12.14.1.
   
2. **Instalación**:
   - Instrucciones para clonar el repositorio y navegar al directorio del proyecto.
   - Instrucciones para instalar las dependencias usando `npm install`.

3. **Ejecución**:
   - Instrucciones para ejecutar el servidor de desarrollo usando `npm run serve`.

4. **Uso**:
   - Información sobre cómo acceder a la aplicación en el navegador.

### Variables de Entorno:
   - Incluye las credenciales específicas de la base de datos MySQL:
     ```env
      NODE_ENV=production
      VUE_APP_TITLE="Prueba Desarrollo Vue - Sandbox"
      VUE_APP_MANIFIEST_TITLE="Prueba Desarrollo Vue  - Sandbox"
      VUE_APP_DESCRIPTION=""
      VUE_APP_PUBLIC_PATH=/prueba-desarrollo-vue
      VUE_APP_BASE_URL=http://127.0.0.1:8000
      VUE_APP_API_URL_BASE=http://127.0.0.1:8000

      VUE_APP_CLIEND_SECRET=oLKExLA5EraJKs8Y1VAlTbPmRsVd0aG7eaIC5g0P
      VUE_APP_CAPTCHA_SITE=6LepndIZAAAAAC0nn8UfZuVA9igVe4bK8fAQyx-u
      VUE_APP_CAPTCHA_SECRET=6LepndIZAAAAAK70HtsXH9upr2MbUs78b9EgJzcC
     ```

Este `README.md` proporciona una guía clara y completa para cualquier desarrollador que desee configurar, ejecutar y contribuir a tu proyecto Vue.js.

