module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended'
  ],
  plugins: [
    'prettier'
  ],
  // add your custom rules here
  rules: {
    'prettier/prettier': 0,
    'object-shorthand': 0,
    'vue/attributes-order': 0,
    'vue/no-template-shadow': 0,
    'vue/no-mutating-props': 0,
    'vue/order-in-components': 0,
    'camelcase': 0
  }
}
