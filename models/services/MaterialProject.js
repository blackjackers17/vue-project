import { Model } from '@/models/Model'
import { Api } from '@/models/Api'

export class MaterialProject extends Model {
  constructor( data = {} ) {
    super(Api.END_POINTS.MATERIAL_PROJECTS(), data)
  }
}
