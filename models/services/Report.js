import { Model } from '@/models/Model'
import { Api } from '@/models/Api'

export class Report extends Model {
  constructor( data = {} ) {
    super(Api.END_POINTS.REPORT(), data)
  }
}