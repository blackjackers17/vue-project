import { Model } from '@/models/Model'
import { Api } from '@/models/Api'

export class Material extends Model {
  constructor( data = {} ) {
    super(Api.END_POINTS.MATERIALS(), data)
  }
}
