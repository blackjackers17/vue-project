import { Model } from '@/models/Model'
import { Api } from '@/models/Api'

export class Location extends Model {
  constructor( data = {} ) {
    super('', data)
  }
  
  getCountries() {
    return this.get(Api.END_POINTS.COUNTRIES());
  }

  getDepartments(countryId) {
    return this.get(Api.END_POINTS.DEPARTMENTS(countryId));
  }

  getCities(departmentId) {
    return this.get(Api.END_POINTS.CITIES(departmentId));
  }
}
