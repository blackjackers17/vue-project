const api = process.env.VUE_APP_API_URL_BASE

export const Api = {
  END_POINTS: {
    PROJECTS: () => `${api}/api/projects`,
    MATERIALS: () => `${api}/api/materials`,
    MATERIAL_PROJECTS: () => `${api}/api/material-projects`,
    COUNTRIES: () => `${api}/api/countries`,
    DEPARTMENTS: (countryId) => `${api}/api/departments/${countryId}`,
    CITIES: (departmentId) => `${api}/api/cities/${departmentId}`,
    REPORT: () => `${api}/api/report`
  },
}
