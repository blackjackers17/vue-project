import { Api } from '@/models/Api'

export const state = () => ({
  // states liquidation period
  periodStart: null,
  periodFinal: null,
  workedDaysGeneral: null,
  settlementMonth: null,

  // states contractors compliance
  contractorsList: [],

})

export const getters = {
  // getters liquidation period
  compliancePeriod: (state) => {
    if (state.periodStart) {
      const [year, month] = state.periodStart.split('-');
      return `${Number(year)}-${Number(month)}`
    }
    return ''
  },
  settlementPeriod: (state) => {
    return state.periodStart + ' AL ' + state.periodFinal
  },
  getMonth: (state) => {
    if (state.settlementMonth) {
      const aux = state.settlementMonth.split('-')
      return parseInt(aux[1])
    } else {
      return 0
    }
  },
  getYear: (state) => {
    if (state.settlementMonth) {
      const aux = state.settlementMonth.split('-')
      return parseInt(aux[0])
    } else {
      return 0
    }
  },
  settlementPeriodStart: (state) => {
    if (state.settlementMonth) {
      return `${state.settlementMonth.substring(
        0,
        4
      )}-${state.settlementMonth.substring(5, 7)}-01`
    } else {
      return null
    }
  },
  settlementPeriodEnd: (state) => {
    if (state.settlementMonth) {
      return `${state.settlementMonth
        ? new Date(
          state.settlementMonth.substring(0, 4),
          +state.settlementMonth.substring(5, 7),
          0
        )
          .toISOString()
          .split('T')[0]
        : null
        }`
    } else {
      return null
    }
  },
  invalidContractorPeriodList: (state, getters) => {
    const result = []
    if (getters.contractorsListGroupByIdentificationAndContract) {
      getters.contractorsListGroupByIdentificationAndContract.forEach(
        (element) => {
          if (
            element.startPeriod < element.start_date.substring(0, 10) ||
            element.startPeriod > element.final_date.substring(0, 10)
          ) {
            result.push(
              'Número de identificación: ' +
              element.identification +
              '. Contrato: ' +
              element.contract_number
            )
          } else if (
            element.finalPeriod < element.start_date.substring(0, 10) ||
            element.finalPeriod > element.final_date.substring(0, 10)
          ) {
            result.push(
              'Número de identificación: ' +
              element.identification +
              '. Contrato: ' +
              element.contract_number
            )
          }
        }
      )
    }
    return result
  },
  isInvalidContractorPeriod: (state, getters) => {
    return getters.invalidContractorPeriodList.length > 0
  },

  // getters contractors
  getContractorById: (state) => (id) => {
    return state.contractorsList.find((con) => con.id === id)
  },
  contractorsListCount: (state) => {
    return state.contractorsList.length
  },
  contractorsListGroupByIdentificationAndContract: (state) => {
    const aux = []
    const result = []
    state.contractorsList.forEach((element) => {
      if (!aux.includes(element.contract_number)) {
        aux.push(element.contract_number)
        result.push(element)
      }
    })
    return result
  },

  getTotalPay: (state) => {
    if (state.contractorsList.length !== 0) {
      let aux = 0
      state.contractorsList.forEach((element) => {
        aux += Math.round(
          (+element.monthly_payment * +element.dias_trabajados) / 30
        )
      })
      return aux
    } else {
      return 0
    }
  },
  getTotalPayByIdentification: (state) => (identificationDoc) => {
    const sum = state.contractorsList
      .filter(({ identification }) => identification === identificationDoc)
      .reduce((accumulator, object) => {
        return (
          accumulator +
          Math.round((object.monthly_payment * object.dias_trabajados) / 30)
        )
      }, 0)
    return sum
  },
  getTotalPayByContractNumber: (state) => (contractNumber) => {
    const sum = state.contractorsList
      // eslint-disable-next-line camelcase
      .filter(({ contract_number }) => contract_number === contractNumber)
      .reduce((accumulator, object) => {
        return (
          accumulator +
          Math.round((object.monthly_payment * object.dias_trabajados) / 30)
        )
      }, 0)
    return sum
  },

  getContractorWithoutBalance: (state) => {
    return state.contractorsList.filter(
      (item) =>
        Math.round((item.monthly_payment * item.dias_trabajados) / 30) >
        +item.balance
    )
  },
}

export const actions = {
  // actions for liquidation period
  updateSettlementMonth({ commit }, value) {
    if (value) {
      commit('UPDATE_SETTLEMENT_MONTH', value)
      commit('UPDATE_PERIOD_FINAL', new Date(value.substring(0, 4), +value.substring(5, 7), 0).toISOString().split('T')[0])
      commit('UPDATE_PERIOD_START', `${value.substring(0, 4)}-${value.substring(5, 7)}-01`)
      commit('UPDATE_DAYS_WORKED_GENERAL', 30)

    }
  },
  updatePeriodStart({ commit }, value) {
    commit('UPDATE_PERIOD_START', value)
  },
  updatePeriodFinal({ commit }, value) {
    commit('UPDATE_PERIOD_FINAL', value)
  },
  updatePeriodContractorItem({ commit }, newUpdate) {
    commit('UPDATE_PERIOD_CONTRACTOR_ITEM', newUpdate)
  },


  updateworkedDaysGeneral({ commit }, value) {
    commit('UPDATE_DAYS_WORKED_GENERAL', value)
  },
  updateWorkedDaysByIdentification({ commit }, newUpdate) {
    commit('UPDATE_WORKER_DAYS_BY_IDENTIFICATION', newUpdate)
  },


  updatePriceContractorItem({ commit }, newUpdate) {
    commit('UPDATE_PRICE_CONTRACTOR_ITEM', newUpdate)
  },

  // actions for contractors compliance
  loadComplianceApprovedList({ state, commit, dispatch }, params) {
    return (
      this.$axios
        .get(Api.END_POINTS.COMPLIANCE_BY_PAYROLL_REVIEW(), { params })
        .then(({ data }) => {
          const aux = []
          data.data.forEach((cont) => {
            const cantidadPmrsPorContrato = data.data.filter(
              (item) => item.contract_number === cont.contract_number
            ).length
            aux.push({
              ...cont,
              saldo: 0,
              dias_trabajados: state.workedDaysGeneral,
              // cuando el contrtato tiene un tipo de RP como Reserva, el pago mensual debe ser igual a la division del valor del componente entre cantidad de detalles
              monthly_payment:
                cont.is_reserve === 'X'
                  ? Math.round(+cont.pay / cantidadPmrsPorContrato)
                  : +cont.pay,
              total_pagar: 0,
              startPeriod: state.periodStart,
              finalPeriod: state.periodFinal,
              balance: +cont.balance,
              pay: +cont.pay,
            })
          })
          commit('SET_CONTRACTORS_LIST', aux)
        })
        .catch((error) => {
          throw error
        })
    )
  },
  clearContractorsList({ commit }) {
    commit('SET_CONTRACTORS_LIST', [])

  }
}

export const mutations = {
  UPDATE_SETTLEMENT_MONTH(state, data) {
    state.settlementMonth = data
  },
  UPDATE_PERIOD_START(state, data) {
    state.periodStart = data
  },
  UPDATE_PERIOD_FINAL(state, data) {
    state.periodFinal = data
  },
  UPDATE_PERIOD_CONTRACTOR_ITEM(state, payload) {
    const indexListAux = state.contractorsList
      .map((e, i) =>
        e.contract_number === payload.contractNumber ? i : undefined
      )
      .filter((x) => x !== undefined)
    if (indexListAux.length !== 0) {
      if (indexListAux.length > 1) {
        indexListAux.forEach((element) => {
          state.contractorsList[element].startPeriod = payload.startPeriod
          state.contractorsList[element].finalPeriod = payload.finalPeriod
        })
      } else {
        state.contractorsList[indexListAux[0]].startPeriod = payload.startPeriod
        state.contractorsList[indexListAux[0]].finalPeriod = payload.finalPeriod
      }
    }
  },


  UPDATE_PRICE_CONTRACTOR_ITEM(state, payload) {
    state.contractorsList[payload.index].monthly_payment = +payload.monthly_payment
  },

  UPDATE_DAYS_WORKED_GENERAL(state, data) {
    state.workedDaysGeneral = data
  },
  UPDATE_WORKER_DAYS_BY_IDENTIFICATION(state, payload) {
    const indexListAux = state.contractorsList
      .map((e, i) =>
        e.contract_number === payload.contractNumber ? i : undefined
      )
      .filter((x) => x !== undefined)
    if (indexListAux.length !== 0) {
      if (indexListAux.length > 1) {
        indexListAux.forEach((element) => {
          state.contractorsList[element].dias_trabajados =
            payload.diasTrabajados
        })
      } else {
        state.contractorsList[indexListAux[0]].dias_trabajados =
          payload.diasTrabajados
      }
    }
  },

  SET_CONTRACTORS_LIST(state, data) {
    state.contractorsList = []
    data.forEach((element) => {
      state.contractorsList.push({
        ...element,
        dias_trabajados: state.workedDaysGeneral,
      })
    })
  },
}
