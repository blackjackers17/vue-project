import { Api } from '@/models/Api'

export const state = () => ({
  // states liquidation period
  periodStart: null,
  periodFinal: null,
  workedDaysGeneral: null,
  settlementMonth: null,

  payrollList: [],
  compliancesList: [],
  payrollDetail: null,

  contractorSevenPayrollList: [],
  contractorSevenPayrollGeneralList: [],

})

export const getters = {
  // getters liquidation period
  compliancePeriod: (state) => {
    if (state.periodStart) {
      const [year, month] = state.periodStart.split('-');
      return `${Number(year)}-${Number(month)}`
    }
    return ''
  },
  settlementPeriod: (state) => {
    return state.periodStart + ' AL ' + state.periodFinal
  },
  getMonth: (state) => {
    if (state.settlementMonth) {
      const aux = state.settlementMonth.split('-')
      return parseInt(aux[1])
    } else {
      return 0
    }
  },
  getYear: (state) => {
    if (state.settlementMonth) {
      const aux = state.settlementMonth.split('-')
      return parseInt(aux[0])
    } else {
      return 0
    }
  },
  settlementPeriodStart: (state) => {
    if (state.settlementMonth) {
      return `${state.settlementMonth.substring(
        0,
        4
      )}-${state.settlementMonth.substring(5, 7)}-01`
    } else {
      return null
    }
  },
  settlementPeriodEnd: (state) => {
    if (state.settlementMonth) {
      return `${state.settlementMonth
        ? new Date(
          state.settlementMonth.substring(0, 4),
          +state.settlementMonth.substring(5, 7),
          0
        )
          .toISOString()
          .split('T')[0]
        : null
        }`
    } else {
      return null
    }
  },

  invalidContractorPeriodList: (state, getters) => {
    const result = []
    if (getters.contractorsListGroupByIdentificationAndContract) {
      getters.contractorsListGroupByIdentificationAndContract.forEach(
        (element) => {
          if (
            element.startPeriod < element.start_date.substring(0, 10) ||
            element.startPeriod > element.final_date.substring(0, 10)
          ) {
            result.push(`Contrato: ${element.contract_number} (${element.start_date.substring(0, 10)} al ${element.final_date.substring(0, 10)})`)
          } else if (
            element.finalPeriod < element.start_date.substring(0, 10) ||
            element.finalPeriod > element.final_date.substring(0, 10)
          ) {
            result.push(`Contrato: ${element.contract_number} (${element.start_date.substring(0, 10)} al ${element.final_date.substring(0, 10)})`)
          }
        }
      )
    }
    return result
  },
  isInvalidContractorPeriod: (state, getters) => {
    return getters.invalidContractorPeriodList.length > 0
  },

  // getters contractors
  getContractorById: (state) => (id) => {
    return state.contractorSevenPayrollList.find((con) => con.id === id)
  },
  contractorsListCount: (state) => {
    return state.contractorSevenPayrollList.length
  },
  contractorsListGroupByIdentificationAndContract: (state) => {
    const aux = []
    const result = []
    state.contractorSevenPayrollList.forEach((element) => {
      if (!aux.includes(element.contract_number)) {
        aux.push(element.contract_number)
        result.push(element)
      }
    })
    return result
  },

  getTotalPay: (state) => {
    if (state.contractorSevenPayrollList.length !== 0) {
      let aux = 0
      state.contractorSevenPayrollList.forEach((element) => {
        aux += Math.round(
          (+element.monthly_payment * +element.dias_trabajados) / 30
        )
      })
      return aux
    } else {
      return 0
    }
  },
  getTotalPayByIdentification: (state) => (identificationDoc) => {
    const sum = state.contractorSevenPayrollList
      .filter(({ identification }) => identification === identificationDoc)
      .reduce((accumulator, object) => {
        return (
          accumulator +
          Math.round((object.monthly_payment * object.dias_trabajados) / 30)
        )
      }, 0)
    return sum
  },
  getTotalPayByContractNumber: (state) => (contractNumber) => {
    const sum = state.contractorSevenPayrollList
      // eslint-disable-next-line camelcase
      .filter(({ contract_number }) => contract_number === contractNumber)
      .reduce((accumulator, object) => {
        return (
          accumulator +
          Math.round((object.monthly_payment * object.dias_trabajados) / 30)
        )
      }, 0)
    return sum
  },

  getContractorWithoutBalance: (state) => {
    return state.contractorSevenPayrollList.filter(
      (item) =>
        Math.round((item.monthly_payment * item.dias_trabajados) / 30) >
        +item.balance
    )
  },


  complianceNotSevenList: (state) => {
    return state.compliancesList.filter((item) => !item.is_seven)
  },
  complianceSevenList: (state) => {
    return state.compliancesList.filter((item) => item.is_seven)
  },
  complianceValidityList: (state, getters) => {
    return getters.complianceSevenList.filter((item) => item.is_validity)
  },
  complianceReserveList: (state, getters) => {
    return getters.complianceSevenList.filter((item) => !item.is_validity)
  },



  contractorSevenPayrollListGroupByIdentificationAndContract: (state) => {
    const aux = []
    const result = []
    state.contractorSevenPayrollList.forEach((element) => {
      if (!aux.includes(element.contract_number)) {
        aux.push(element.contract_number)
        result.push(element)
      }
    })
    return result
  },

  entrysDescription: (state) => {
    const aux = []
    state.contractorSevenPayrollList.forEach((element) => {
      if (!aux.includes(element.entry)) aux.push(element.entry)
    })
    const auxEntrySimple = []
    aux.forEach((item) => {
      auxEntrySimple.push(item.substring(item.length - 4, item.length))
    })
    return auxEntrySimple.join(' - ')
    // return aux.join(' , ')
  },
  fundingSourceDescription: (state) => {
    const aux = []
    state.contractorSevenPayrollList.forEach((element) => {
      if (!aux.includes(element.source)) aux.push(element.source)
    })
    return aux.join(' , ')
  },


}
export const actions = {
  // actions for liquidation period
  updateSettlementMonth({ commit }, value) {
    console.log('si esta llegando desde plani', value)
    if (value) {
      commit('UPDATE_SETTLEMENT_MONTH', value)
      commit('UPDATE_PERIOD_FINAL', new Date(value.substring(0, 4), +value.substring(5, 7), 0).toISOString().split('T')[0])
      commit('UPDATE_PERIOD_START', `${value.substring(0, 4)}-${value.substring(5, 7)}-01`)
      commit('UPDATE_DAYS_WORKED_GENERAL', 30)
    }
  },
  updatePeriodStart({ commit }, value) {
    commit('UPDATE_PERIOD_START', value)
  },
  updatePeriodFinal({ commit }, value) {
    commit('UPDATE_PERIOD_FINAL', value)
  },
  updatePeriodContractorItem({ commit }, newUpdate) {
    commit('UPDATE_PERIOD_CONTRACTOR_ITEM', newUpdate)
  },


  updateworkedDaysGeneral({ commit }, value) {
    commit('UPDATE_DAYS_WORKED_GENERAL', value)
  },
  updateWorkedDaysByIdentification({ commit }, newUpdate) {
    commit('UPDATE_WORKER_DAYS_BY_IDENTIFICATION', newUpdate)
  },
  updatePriceContractorItem({ commit }, newUpdate) {
    commit('UPDATE_PRICE_CONTRACTOR_ITEM', newUpdate)
  },


  loadComplianceApprovedList({ state, commit, dispatch }, params) {
    return (
      this.$axios
        .get(Api.END_POINTS.COMPLIANCE_BY_PAYROLL_REVIEW(), { params })
        .then(({ data }) => {
          commit('SET_COMPLIANCES_LIST', data.data)
        })
        .catch((error) => {
          throw error
        })
    )
  },
  clearComplianceList({ commit }) {
    commit('SET_COMPLIANCES_LIST', [])
  },
  getPayrollByPeriod({ state, commit, dispatch, rootState }) {
    return (
      this.$axios
        .get(`${Api.END_POINTS.PAYROLL()}/get-payroll-by-period?period=${rootState.compliance.settlementMonth}`)
        .then(({ data }) => {
          commit('SET_PAYROLL_LIST', data.data)
        })
        .catch((error) => {
          throw error
        })
    )
  },
  createPayroll({ commit, dispatch }, dto) {
    return this.$axios
      .post(`${Api.END_POINTS.PAYROLL()}/save-payroll`, dto)
      .then(({ data }) => {
        return data
      })
      .catch((error) => {
        console.log(error)
        this.$snackbar({ message: 'Oops. Ocurrio un error mientras se creaba el certificado' })
        // throw error
      })
  },
  savePmrCompliancesPmr({ commit, dispatch }, dto) {
    return this.$axios
      .post(`${Api.END_POINTS.PAYROLL()}/save-pmr-compliances`, dto)
      .then(({ data }) => {
        console.log('planilla actualizada')
        this.$snackbar({ message: 'El certificado se envio al área financiera para su revsión.', color: 'success' })
        console.log(data)
      })
      .catch((error) => {
        console.log(error)
        this.$snackbar({ message: 'Oops. Ocurrio un error mientras se actualiza el certificado' })
        // throw error
      })
  },

  loadPayrollDetail({ commit, dispatch }, id) {
    return this.$axios
      .get(`${Api.END_POINTS.PAYROLL()}/get-payroll-detail-by-id?id=${id}`)
      .then(({ data }) => {
        commit('SET_PAYROLL_DETAIL', data.data)
        const auxPeriod = +data.data.month < 10 ? `${data.data.year}-0${data.data.month}` : `${data.data.year}-${data.data.month}`
        dispatch('updateSettlementMonth', auxPeriod)
        commit('SET_CONTRACTOR_SEVEN_PAYROLL_LIST')
      })
      .catch((error) => {
        console.log(error)
        this.$snackbar({ message: 'Oops. Ocurrio un error cargand los datos del certificado' })
        // throw error
      })
  },

  loadContractorsSevenGeneralList({ commit }, value) {
    commit('SET_CONTRACTORS_SEVEN_GENERAL_LIST', value)
  },
}

export const mutations = {
  UPDATE_SETTLEMENT_MONTH(state, data) {
    state.settlementMonth = data
  },
  UPDATE_PERIOD_START(state, data) {
    state.periodStart = data
  },
  UPDATE_PERIOD_FINAL(state, data) {
    state.periodFinal = data
  },
  UPDATE_PERIOD_CONTRACTOR_ITEM(state, payload) {
    const indexListAux = state.contractorSevenPayrollList
      .map((e, i) =>
        e.contract_number === payload.contractNumber ? i : undefined
      )
      .filter((x) => x !== undefined)
    if (indexListAux.length !== 0) {
      if (indexListAux.length > 1) {
        indexListAux.forEach((element) => {
          state.contractorSevenPayrollList[element].startPeriod = payload.startPeriod
          state.contractorSevenPayrollList[element].finalPeriod = payload.finalPeriod
        })
      } else {
        state.contractorSevenPayrollList[indexListAux[0]].startPeriod = payload.startPeriod
        state.contractorSevenPayrollList[indexListAux[0]].finalPeriod = payload.finalPeriod
      }
    }
  },
  UPDATE_PRICE_CONTRACTOR_ITEM(state, payload) {
    state.contractorSevenPayrollList[payload.index].monthly_payment = +payload.monthly_payment
  },

  UPDATE_DAYS_WORKED_GENERAL(state, data) {
    state.workedDaysGeneral = data
  },
  UPDATE_WORKER_DAYS_BY_IDENTIFICATION(state, payload) {
    const indexListAux = state.contractorSevenPayrollList
      .map((e, i) =>
        e.contract_number === payload.contractNumber ? i : undefined
      )
      .filter((x) => x !== undefined)
    if (indexListAux.length !== 0) {
      if (indexListAux.length > 1) {
        indexListAux.forEach((element) => {
          state.contractorSevenPayrollList[element].dias_trabajados =
            payload.diasTrabajados
        })
      } else {
        state.contractorSevenPayrollList[indexListAux[0]].dias_trabajados =
          payload.diasTrabajados
      }
    }
  },



  SET_COMPLIANCES_LIST(state, data) {
    state.compliancesList = data
  },
  SET_PAYROLL_LIST(state, data) {
    state.payrollList = data
  },
  SET_PAYROLL_DETAIL(state, data) {
    state.payrollDetail = data
  },
  SET_CONTRACTOR_SEVEN_PAYROLL_LIST(state) {
    const auxSevenList = []
    if (state.payrollDetail !== null) {
      state.payrollDetail.compliances.forEach(complianceItem => {
        if (complianceItem.data_seven !== null) {


          // auxSevenList.push({
          //   compliance_id: complianceItem.compliance_id,
          //   ...complianceItem.data_seven,
          // });

          complianceItem.data_seven.forEach(cont => {
            const cantidadPmrsPorContrato = complianceItem.data_seven.filter(
              (item) => item.contract_number === cont.contract_number
            ).length
            auxSevenList.push({
              compliance_id: complianceItem.compliance_id,
              ...cont,
              saldo: 0,
              dias_trabajados: state.workedDaysGeneral,
              // cuando el contrtato tiene un tipo de RP como Reserva, el pago mensual debe ser igual a la division del valor del componente entre cantidad de detalles
              // cuando el contrtato tiene un tipo de RP como Reserva, el pago mensual debe ser igual a la division del valor del componente entre cantidad de detalles
              monthly_payment:
                cont.is_reserve === 'X'
                  ? Math.round(+cont.pay / cantidadPmrsPorContrato)
                  : +cont.pay,
              total_pagar: 0,
              startPeriod: state.periodStart,
              finalPeriod: state.periodFinal,
              balance: +cont.balance,
              pay: +cont.pay,
            })
            // auxSevenList.push(contractSevenItem);
          })
        }
      });
    }
    state.contractorSevenPayrollList = auxSevenList
  },
  SET_CONTRACTORS_SEVEN_GENERAL_LIST(state, data) {
    console.log(data)
    state.contractorSevenPayrollGeneralList = data
  },
}
