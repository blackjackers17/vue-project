// plugins/filters.js

import Vue from 'vue'

Vue.filter('currencyFormat', function (value) {
  if (!value) return ''
  const formattedValue = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  return '$' + formattedValue
})
