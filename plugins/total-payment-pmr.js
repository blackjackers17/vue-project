export default (context, inject) => {
    const aux = (item) =>
        Math.round((item.monthly_payment * item.dias_trabajados) / 30)
    inject('totalPaymentPmr', aux)
}
