export default (context, inject) => {
    const precio = (valor) =>
        (valor / 1)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    inject('precio', precio)
}
