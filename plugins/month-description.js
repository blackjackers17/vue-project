export default (context, inject) => {
  const date = new Date()

  const monthDescription = (monthNumber) => {
    date.setMonth(monthNumber - 1)
    return date.toLocaleString('es-ES', {
      month: 'long',
    })
  }
  inject('monthDescription', monthDescription)
}
